<?php

use App\Http\Controllers\Admin\FileController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LocalizationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\HomeController@index')->name("home.index");
Route::get('/about', 'App\Http\Controllers\HomeController@about')->name("home.about");
Route::get('/products', 'App\Http\Controllers\ProductController@index')->name("product.index");
Route::get('/products/{id}', 'App\Http\Controllers\ProductController@show')->name("product.show");

Route::get('/cart', 'App\Http\Controllers\CartController@index')->name("cart.index");
Route::get('/cart/delete', 'App\Http\Controllers\CartController@delete')->name("cart.delete");
Route::post('/cart/add/{id}', 'App\Http\Controllers\CartController@add')->name("cart.add");

Route::get('change', [LocalizationController::class, 'change'])->name('changeLang');

Route::middleware('auth')->group(function () {
    Route::get('/cart/purchase', 'App\Http\Controllers\CartController@purchase')->name("cart.purchase");
    Route::get('/my-account/orders', 'App\Http\Controllers\MyAccountController@orders')->name("myaccount.orders");
});

Route::middleware('admin')->group(function () {
    Route::get('/admin', 'App\Http\Controllers\Admin\AdminHomeController@index')->name("admin.home.index");
    Route::get('/admin/products', 'App\Http\Controllers\Admin\AdminProductController@index')->name("admin.product.index");
    Route::post('/admin/products/store', 'App\Http\Controllers\Admin\AdminProductController@store')->name("admin.product.store");
    Route::delete('/admin/products/{id}/delete', 'App\Http\Controllers\Admin\AdminProductController@delete')->name("admin.product.delete");
    Route::get('/admin/products/{id}/edit', 'App\Http\Controllers\Admin\AdminProductController@edit')->name("admin.product.edit");
    Route::put('/admin/products/{id}/update', 'App\Http\Controllers\Admin\AdminProductController@update')->name("admin.product.update");
    Route::resource('/admin/categories','App\Http\Controllers\Admin\AdminCategoryController');
    Route::resource('/admin/fornisseurs','App\Http\Controllers\Admin\AdminFornisseurController');
    // Route::get('/admin/products/importation-fichiers', [FileController::class, 'importExportView']);
    Route::get('/admin/products/export', [FileController::class, 'export'])->name('export');
    Route::post('/admin/products/import', [FileController::class, 'import'])->name('import');
    Route::get('/admin/discounts', 'App\Http\Controllers\Admin\AdminDiscountsController@index')->name("admin.discounts.index");
     Route::post('/admin/discounts/store','App\Http\Controllers\Admin\AdminDiscountsController@store')->name('admin.discounts.store');
     Route::get('/admin/discounts/{discount}/edit','App\Http\Controllers\Admin\AdminDiscountsController@edit')->name('admin.discounts.edit');
     Route::put('/admin/discounts/{discount}', 'App\Http\Controllers\Admin\AdminDiscountsController@update')->name('admin.discounts.update');
     Route::delete('/admin/discounts/{discount}', 'App\Http\Controllers\Admin\AdminDiscountsController@destroy')->name('admin.discounts.destroy');



     Route::get('/admin/orders', 'App\Http\Controllers\Admin\OrderController@index')->name("admin.orders.index");
     Route::post('/admin/orders/store','App\Http\Controllers\Admin\OrderController@store')->name('admin.orders.store');
     Route::get('/admin/orders/{id}/edit','App\Http\Controllers\Admin\OrderController@edit')->name('admin.orders.edit');
     Route::put('/admin/orders/{id}/update','App\Http\Controllers\Admin\OrderController@update')->name('admin.orders.update');
     Route::delete('/admin/orders/{order}', 'App\Http\Controllers\Admin\OrderController@destroy')->name('admin.orders.destroy');


});

Route::resource('/superAdmin/users','App\Http\Controllers\Admin\AdminUserController')->middleware(['superAdmin']);

Auth::routes();
