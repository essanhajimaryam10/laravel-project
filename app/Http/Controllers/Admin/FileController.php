<?php

namespace App\Http\Controllers\Admin;
    
use Illuminate\Http\Request;
use App\Exports\ProductsExport;
use App\Imports\ProductsImport;
use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\Controller;
use App\Models\Product;

class FileController extends Controller
{
    public function importExportView()
    {
       return view('admin.product.index');
    }
     
    /**
    * @return \Illuminate\Support\Collection
    */
    public function export() 
    {
        return Excel::download(new ProductsExport, 'products.xlsx');
    }
     
    /**
    * @return \Illuminate\Support\Collection
    */
    public function import(Request $request) 
    {
        $file = $request->file('file');
       
        $fileName = $file->getClientOriginalName();
        $file->move(storage_path('app'), $fileName);

        $data = Excel::toArray([], $fileName)[0];
        foreach ($data as $row) {
            $product = new Product([
                'name' => $row[1],
                'description' => $row[2],
                'image' => $row[3],
                'price' => $row[4],
                'category_id' => $row[7],
                'Quantity' => $row[8],
                'fornisseur_id' =>$row[9],

            ]);
            $product->save();
        }
        return redirect('/admin/products')->with('success', 'Excel data imported successfully.');
    }
//
}
