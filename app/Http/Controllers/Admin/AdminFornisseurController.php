<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Fornisseur;
use Illuminate\Http\Request;

class AdminFornisseurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $viewData = [];
        $viewData["title"] = "Admin Page - Fornisseur - Online Store";
        $viewData["fornisseur"] = Fornisseur::paginate(3);
        return view('admin.fornisseur.index')->with("viewData", $viewData);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
            Fornisseur::validate($request);
    
            $newFornisseur = new Fornisseur();
            $newFornisseur->setRaison_social($request->input('raison_social'));
            $newFornisseur->setAdresse($request->input('adresse'));
            $newFornisseur->setEmail($request->input('email'));
            $newFornisseur->setTel($request->input('tel'));
            $newFornisseur->setDescription($request->input('description'));
            $newFornisseur->save();
    
    
            return back();
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $viewData = [];
        $viewData["title"] = "Admin Page - Edit fornisseur - Online Store";
        $viewData["fornisseur"] = Fornisseur::findOrFail($id);
        return view('admin.fornisseur.edit')->with("viewData", $viewData);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Fornisseur::validate($request);

        $newFornisseur = Fornisseur::findOrFail($id);
        $newFornisseur->setRaison_social($request->input('raison_social'));
        $newFornisseur->setEmail($request->input('email'));
        $newFornisseur->setAdresse($request->input('adresse'));
        $newFornisseur->setTel($request->input('tel'));
        $newFornisseur->setDescription($request->input('description'));
    

      
        $newFornisseur->save();
        return redirect()->route('fornisseurs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Fornisseur::destroy($id);
        return back();
    }
}
