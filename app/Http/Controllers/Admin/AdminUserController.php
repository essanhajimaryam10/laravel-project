<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;

class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $viewData = [];
        $viewData["title"] = "Admin Page - Users - Online Store";
        $viewData["users"] = User::paginate(2);
        $viewData["roles"] = User::select('role')->distinct()->get();
        return view('admin.users.index')->with("viewData", $viewData);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response**/
     public function store(Request $request)
    {
        
            // User::validate($request);
    
            $newUser = new User();
            $newUser->setName($request->input('name'));
            $newUser->setEmail($request->input('email'));
            $newUser->setPassword($request->input('password'));
            $newUser->setRole($request->input('role'));
            $newUser->setBalance($request->input('balance'));   
            $newUser->save();
            return redirect()->route('users.index');
        
    }

    
     /* Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $viewData = [];
        $viewData["title"] = "Admin Page - Edit User - Online Store";
        $viewData["user"] = User::findOrFail($id);
        return view('admin.users.edit')->with("viewData", $viewData);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // User::validate($request);

        $user = User::findOrFail($id);
        $user->setName($request->input('name'));
        $user->setEmail($request->input('email'));
        $user->setPassword($request->input('password'));
        $user->setRole($request->input('role'));
        $user->setBalance($request->input('balance'));

        $user->save();
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return back();
    }
}
