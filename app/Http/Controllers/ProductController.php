<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Solde_category;
use Symfony\Component\Console\Input\Input;

class ProductController extends Controller
{

    public function index(Request $request)
    {
        $viewData = [];
        $viewData["title"] = "Products - Online Store";
        $viewData["subtitle"] =  "List of products";
        $viewData["categories"] = Category::all();
        $viewData["category"] = $request->query('category');        
        $category = $request->input('category_id');
        $viewData["cat_id"] = $category;
        if ($category) {
            $viewData["products"] = Product::whereHas(
                'Category',
                function ($query) use ($category) {
                    $query->where('id', $category);
                }
            )->paginate(10);
        } else {
            $viewData["products"] = Product::paginate(10);
        }

        return view('product.index')->with("viewData", $viewData);
    }


 


   

    public function show($id)
    {
        $viewData = [];
        $product = Product::findOrFail($id);
        $viewData["title"] = $product->getName() . " - Online Store";
        $viewData["subtitle"] =  $product->getName() . " - Product information";
        $viewData["product"] = $product;
        return view('product.show')->with("viewData", $viewData);
    }
    // public function quantity_after_purchase( $product ){
    //     $Qte=Order::class;
    //     if($Qte==true){
    //         $qtevendé= session('products')[$product->getId()];
    //         $newQteProduct =new Product();
    //         $newQteProduct->getQuantity_store();
    //         $newQteProduct-$qtevendé;
    //         return view('cart.index');
    //     }
    // }
    
   
}
