<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'value',
        'start_date',
        'end_date',
        'apply_to'
    ];

    protected $dates = ['start_date', 'end_date'];

    // Relationships

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
    public function getId()
    {
        return $this->attributes["id"];
    }
    public function getName()
    {
        return $this->attributes["name"];
    }
   
    public function getValue()
    {
        return $this->attributes["value"];
    }
    public function getStartDate()
    {
        return $this->attributes["start_date"];
    }
    public function getEndDate()
    {
        return $this->attributes["end_date"];
    }
    public function getApplyTo()
    {
        return $this->attributes["apply_to"];
    }

    // Accessors
    public function getFormattedValueAttribute()
    {
        return '$' . number_format($this->attributes['value'], 2);

    }

    // Mutators
    public function setValueAttribute($value)
    {
        $this->attributes['value'] = (float) str_replace(',', '.', $value);
    }
    public function getCategoryIds()
    {
        return $this->categories()->pluck('category_id')->toArray();
    }
    public function getProductIds()
    {
        return $this->products()->pluck('product_id')->toArray();
    }

}

