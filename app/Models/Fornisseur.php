<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fornisseur extends Model
{
    use HasFactory;
    protected $fillable = ["raison_social","tel","email","adresse","description"];

    public static function validate($request)
    {
        $request->validate([
            "raison_social" => "required",
            "tel" => "required",
            "email" => "required",
            "adresse" => "required",
            "description" => "required",
        ]);
    }
    public function getId()
    {
        return $this->attributes['id'];
    }

    public function setId($id)
    {
        $this->attributes['id'] = $id;
    }
    public function getRaison_social()
    {
        return $this->attributes['raison_social'];
    }

    public function setRaison_social($raison_social)
    {
        $this->attributes['raison_social'] = $raison_social;
    }

    public function getTel()
    {
        return $this->attributes['tel'];
    }

    public function setTel($tel)
    {
        $this->attributes['tel'] = $tel;
    }
    public function getEmail()
    {
        return $this->attributes['email'];
    }

    public function setEmail($email)
    {
        $this->attributes['email'] = $email;
    }
    public function getAdresse()
    {
        return $this->attributes['adresse'];
    }

    public function setAdresse($adresse)
    {
        $this->attributes['adresse'] = $adresse;
    }
    public function getDescription()
    {
        return $this->attributes['description'];
    }

    public function setDescription($description)
    {
        $this->attributes['description'] = $description;
    }
    public function getCreatedAt()
    {
        return $this->attributes['created_at'];
    }

    public function setCreatedAt($createdAt)
    {
        $this->attributes['created_at'] = $createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->attributes['updated_at'];
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->attributes['updated_at'] = $updatedAt;
    }

}
