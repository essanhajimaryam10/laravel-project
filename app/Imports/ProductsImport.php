<?php

namespace App\Imports;

use App\Models\Product;
use Maatwebsite\Excel\Concerns\ToModel;

class ProductsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Product([
           
              
                "name" => $row['name'],
                "description" => $row['description'],
                'image' => $row['image'],
                "price" => $row['price'],
                "category" => $row['category'],
                'Quantity' => $row['Quantity'],
                'fornisseur_id' =>$row['fornisseur_id'],
            
        ]);
    }
}
