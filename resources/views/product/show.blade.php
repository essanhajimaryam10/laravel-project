@extends('layouts.app')
@section('title', $viewData["title"])
@section('subtitle', $viewData["subtitle"])
@section('content')
<div class="card mb-3">
  <div class="row g-0">
    <div class="col-md-4">
      <img src="{{ asset('/storage/'.$viewData['product']->getImage()) }}" class="img-fluid rounded-start">
    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h3 class="card-title">
        @if ($viewData['product']->hasActiveDiscount())
            <del>{{ $viewData['product']->getPrice() }}DH</del>
            <strong style="color:red">{{ $viewData['product']->getSalePrice() }} DH</strong>
        @else
            <strong>{{ $viewData['product']->getPrice() }}DH</strong>
        @endif  
        </h3>
        <p class="card-text">{{ $viewData["product"]->getDescription() }}</p>
        <p class="btn btn-secondary">Quantity en stock: {{ $viewData["product"]->getQuantityStore() }}</p>
        <p class="card-text">
        <form method="POST" action="{{ route('cart.add', ['id'=> $viewData['product']->getId()]) }}">
          <div class="row">
            @csrf
            <div class="col-auto">
              <div class="input-group col-auto">
                <div class="input-group-text">Quantity</div>
                <input type="number" min="1" max={{ $viewData["product"] ->getQuantityStore() }} class="form-control quantity-input" name="quantity" value="1">
              </div>
            </div>
            <div class="col-auto">
              <button class="btn bg-primary text-white" type="submit">Add to cart</button>
            </div>
          </div>
        </form>
        </p>
      </div>
    </div>
  </div>
</div>
@endsection
