@extends('layouts.app')
@section('title', $viewData["title"])
@section('subtitle', $viewData["subtitle"])
@section('content')
<div class="row">
 
  <div class="col-md-4 col-lg-3 mb-2">
    
      <form action="{{route('product.index')}}" method="get" class="text-center d-flex">
        @csrf
        <select name="category_id"  class="form-select " style="display:inline" >
          <option value="{{null}}" selected>all</option>
          @foreach($viewData['categories'] as $index=>$category)
          <option  value="{{$category->getId()}}" {{$category->getId() == $viewData["cat_id"] ? "selected": ""}}>
            {{$category->getName()}}
          </option>
          @endforeach
        </select>
        <button type="submit" class="btn bg-primary text-white  mt-3 form_controle" style="display:inline">filtrer</button>
      </form>
  </div>
  
</div>

        
<div class="row img-responsive h-100">
  @foreach ($viewData["products"] as $product)
  
  <div class="col-md-4 col-lg-3  mb-2 ">
    <div class="card">
      <img src="{{ asset('/storage/'.$product->getImage()) }}" class="card-img-top img-card ">
      <div class="card-body text-center">
        <a href="{{ route('product.show', ['id'=> $product->getId()]) }}"
          class="btn bg-primary text-white">{{ $product->getName() }}</a>
      </div>
      <div class="card">
        <p>
          <strong>Prix</strong> 
            @if ($product->hasActiveDiscount())
              <del>{{ $product->getPrice() }} DH</del>
              <strong style="color:red">{{ $product->getSalePrice() }}DH</strong>
            @else
              <strong>{{ $product->getPrice() }}DH</strong>
            @endif
        </p>
      </div>
     
          @if($product->getQuantityStore() == 0 )
          <div class="btn bg-danger d-flex m-2">Out of stock</div>
          @endif
      </div>
    </div>
  
  
  @endforeach
  </div>
 {{$viewData["products"]->appends(Request::except('page'))->render() }}

</div>
@endsection

