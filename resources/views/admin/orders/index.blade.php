@extends('layouts.admin')
@section('title', $viewData["title"])
@section('content')
<!-- 
<div class="card mb-4">
    <div class="card-header"> {{$viewData['title']}} </div>
    <div class="card-body">
        @if ($errors->any())
        <ul class="alert alert-danger list-unstyled">
            @foreach ($errors->all() as $error)
            <li>- {{ $error }}</li>
            @endforeach
        </ul>
        @endif

        <form method="POST" action="{{ route('admin.orders.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col">
                    <div class="mb-3 row">
                        <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Phone:</label>
                        <div class="col-lg-10 col-md-6 col-sm-12">
                            <input name="phone" value="{{ old('phone') }}" type="text" class="form-control">
                        </div>
                    </div>
                   
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="mb-3 row">
                        <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Adress:</label>
                        <div class="col-lg-10 col-md-6 col-sm-12">
                            <input name="adress" value="{{ old('adress') }}" type="text" class="form-control">
                        </div>
                    </div>
                   
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="mb-3 row">
                        <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Date:</label>
                        <div class="col-lg-10 col-md-6 col-sm-12">
                            <input name="date" value="{{ old('date') }}" type="date" class="form-control">
                        </div>
                    </div>
                   
                </div>
            </div>


        

        

            <div class="form-group">
                <label for="status">Status :</label>
                <select name="status" id="status" class="form-control">
                    <option value="Emballé">Emballé</option>
                    <option value="Envoyé">Envoyé</option>
                    <option value="En route">En route</option>
                    <option value="Recu">Recu</option>
                    <option value="Retournée"> Retournée</option>
                    <option value="fermée"> fermée</option>
                </select>
            </div>


            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div> -->
<div class="card">
    <div class="card-header">
        Manage {{$viewData['title']}}
    </div>
    <div class="card-body ">
        <table class="table table-bordered table-striped ">
            <thead>
                <tr>
                    <th>ID Order</th>
                    <th>Client Name</th>
                    <th>Items(quantity , price)</th>
                    <th>Total</th>

                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>

                @foreach ($viewData['orders'] as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->user->name }}</td>
                    <td>
                        @foreach($order->items as $item )
                        {{ $item->quantity}} and {{ $item->price   }}Dh <br>

                        @endforeach
                    </td>
                    <td>{{$order->total}}</td>
                    <td> {{$order->status}}</td>
                    <td><a class="btn btn-primary" href="{{ route('admin.orders.edit', $order) }}"><i class="bi-pencil"></i></a></td>
                </tr>
                @endforeach



            </tbody>
        </table>
    </div>
</div>

@endsection