@extends('layouts.admin')
@section('title', $viewData["title"])
@section('content')
<div class="card mb-4">
    <div class="card-header">
        Edit Order
    </div>
    <div class="card-body">
        @if($errors->any())
        <ul class="alert alert-danger list-unstyled">
            @foreach($errors->all() as $error)
            <li>- {{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <form method="POST" action="{{ route('admin.orders.update',  ['id'=> $viewData['order']->getId()]) }}" >
            @csrf
            @method('PUT')

            <div class="form-group">
            <label for="name">Order id </label>
            <input type="text" name="id" id="id" class="form-control" value="{{ $viewData['order']->id }}" required readonly >
        </div>
            <label for="status">status :</label>
            <select name="status" id="status" class="form-control">
                <option value="Emballé" {{   $viewData['order']->status == 'Emballé' ? 'selected' : '' }}>Emballé</option>
                <option value="Envoyé" {{   $viewData['order']->status == 'Envoyé' ? 'selected' : '' }}>Envoyé</option>
                <option value="En route" {{   $viewData['order']->status == 'En route' ? 'selected' : '' }}>En route</option>
                <option value="Recu" {{   $viewData['order']->status == 'Recu' ? 'selected' : '' }}>Recu</option>
                <option value="Retournée" {{   $viewData['order']->status == 'Retournée' ? 'selected' : ''}}> Retournée</option>
                <option value="fermée" {{   $viewData['order']->status == 'fermée' ? 'selected' : ''}}> fermée</option>
            </select>





            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
</div>
@endsection