@extends('layouts.admin')
@section('title', $viewData["title"])
@section('content')
<div class="card mb-4">
  <div class="card-header">
    Create Users
  </div>
  <div class="card-body">
    @if($errors->any())
    <ul class="alert alert-danger list-unstyled">
      @foreach($errors->all() as $error)
      <li>- {{ $error }}</li>
      @endforeach
    </ul>
    @endif

    <form method="POST" action="{{ route('users.store') }}" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Name:</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="name" value="{{ old('name') }}" type="text" class="form-control">
            </div>
          </div>
        </div>
       
      </div>
      <div class="row">
       
        <div class="col">
          &nbsp;
        </div>
      </div>
      <div class="col">
        <div class="mb-3 row">
          <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Email:</label>
          <div class="col-lg-10 col-md-6 col-sm-12">
            <input name="email" value="{{ old('email') }}" type="email" class="form-control">
          </div>
        </div>
      </div>
      <div class="col">
        <div class="mb-3 row">
          <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Password:</label>
          <div class="col-lg-10 col-md-6 col-sm-12">
            <input name="password" value="{{ old('password') }}" type="password" class="form-control">
          </div>
        </div>
      </div>
      <div class="col">
        <div class="mb-3 row">
          <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Role:</label>
          <div class="col-lg-10 col-md-6 col-sm-12">
            <select name="role" >
              <option value="{{null}}" >select role</option>
             
              <option >
                admin 
              </option>
              <option >
              super_admin 
              </option>
              <option >
                client
                </option>
            </select>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="mb-3 row">
          <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Balance:</label>
          <div class="col-lg-10 col-md-6 col-sm-12">
            <input name="balance" value="{{ old('balance') }}" type="number" class="form-control">
          </div>
        </div>
      </div>
      
      
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
  
</div>

<div class="card">
  <div class="card-header">
    Manage Users
  </div>
  <div class="card-body">
    <table class="table table-bordered table-striped">
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col">Name</th>
          <th scope="col">Role</th>
          <th scope="col">Edit</th>
          <th scope="col">Delete</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($viewData["users"] as $user)
        <tr>
          <td>{{ $user->getId() }}</td>
          <td>{{ $user->getRole() }}</td>
          <td>{{ $user->getName() }}</td>
          <td>
            <a class="btn btn-primary" href="{{route('users.edit', ['user'=> $user->getId()])}}">
              <i class="bi-pencil"></i>
            </a>
          </td>
          <td>
            <form action="{{ route('users.destroy', $user->getId())}}" method="POST">
              @csrf
              @method('DELETE')
              <button class="btn btn-danger">
                <i class="bi-trash"></i>
              </button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$viewData["users"]->links()}}

  </div>
</div>
@endsection
