@extends('layouts.admin')
@section('title', $viewData["title"])
@section('content')
<div class="card mb-4">
  <div class="card-header">
    Add fornisseurs
  </div>
  <div class="card-body">
    @if($errors->any())
    <ul class="alert alert-danger list-unstyled">
      @foreach($errors->all() as $error)
      <li>- {{ $error }}</li>
      @endforeach
    </ul>
    @endif

    <form method="POST" action="{{ route('fornisseurs.store') }}" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Raison social:</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="raison_social" value="{{ old('raison_social') }}" type="text" class="form-control">
            </div>
          </div>
        </div>
       
      </div>
      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Email </label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="email" value="{{ old('email') }}" type="text" class="form-control">
            </div>
          </div>
        </div>
       
      </div>
      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Adresse:</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="adresse" value="{{ old('adresse') }}" type="text" class="form-control">
            </div>
          </div>
        </div>
       
      </div>
      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Telephone:</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="tel" value="{{ old('tel') }}" type="text" class="form-control">
            </div>
          </div>
        </div>
       
      </div>
      <div class="row">
       
        <div class="col">
          &nbsp;
        </div>
      </div>
      <div class="mb-3">
        <label class="form-label">Description</label>
        <textarea class="form-control" name="description" rows="3">{{ old('description') }}</textarea>
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
</div>

<div class="card">
  <div class="card-header">
    Manage Fornisseurs
  </div>
  <div class="card-body">
    <table class="table table-bordered table-striped">
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col">Email</th>
          <th scope="col">Adresse</th>
          <th scope="col">Raison</th>
          <th scope="col">Telephone</th>
          <th scope="col">Edit</th>
          <th scope="col">Delete</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($viewData["fornisseur"] as $fornisseur)
        <tr>
          <td>{{ $fornisseur->getId() }}</td>
          <td>{{ $fornisseur->getEmail() }}</td>
          <td>{{ $fornisseur->getAdresse() }}</td>
          <td>{{ $fornisseur->getRaison_social() }}</td>
          <td>{{ $fornisseur->getTel() }}</td>
          <td>
            <a class="btn btn-primary" href="{{route('fornisseurs.edit', ['fornisseur'=> $fornisseur->getId()])}}">
              <i class="bi-pencil"></i>
            </a>
          </td>
          <td>
            <form action="{{ route('fornisseurs.destroy', $fornisseur->getId())}}" method="POST">
              @csrf
              @method('DELETE')
              <button class="btn btn-danger">
                <i class="bi-trash"></i>
              </button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$viewData["fornisseur"]->links()}}
  </div>
</div>
@endsection
