@extends('layouts.admin')
@section('title', $viewData["title"])
@section('content')
<div class="card mb-4">
  <div class="card-header">
    Edit Category
  </div>
  <div class="card-body">
    @if($errors->any())
    <ul class="alert alert-danger list-unstyled">
      @foreach($errors->all() as $error)
      <li>- {{ $error }}</li>
      @endforeach
    </ul>
    @endif

    <form method="POST" action="{{ route('fornisseurs.update', ['fornisseur'=> $viewData['fornisseur']->getId()]) }}"
      enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Raison social:</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="raison_social" value="{{ $viewData['fornisseur']->getRaison_social() }}" type="text" class="form-control">
            </div>
          </div>
        </div>
      
      </div>
      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Email:</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="email" value="{{ $viewData['fornisseur']->getEmail() }}" type="text" class="form-control">
            </div>
          </div>
        </div>
      
      </div>
      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Telephone:</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="tel" value="{{ $viewData['fornisseur']->getTel() }}" type="text" class="form-control">
            </div>
          </div>
        </div>
      
      </div>
      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Adresse:</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="adresse" value="{{ $viewData['fornisseur']->getAdresse() }}" type="text" class="form-control">
            </div>
          </div>
        </div>
      
      </div>
      
      <div class="row">
        
        <div class="col">
          &nbsp;
        </div>
      </div>
      <div class="mb-3">
        <label class="form-label">Description</label>
        <textarea class="form-control" name="description"rows="3">{{ $viewData['fornisseur']->getDescription() }}</textarea>
      </div>
      <button type="submit" class="btn btn-primary">Edit</button>
    </form>
  </div>
</div>
@endsection
