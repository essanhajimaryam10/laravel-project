@extends('layouts.admin')
@section('title', $viewData["title"])
@section('content')
    
    <div class="card mb-4">
        <div class="card-header"> {{$viewData['title']}} </div>
        
        <div class="card-body">
        @if ($errors->any())
                <ul class="alert alert-danger list-unstyled">
                    @foreach ($errors->all() as $error)
                        <li>- {{ $error }}</li>
                    @endforeach
                </ul>
            @endif



    <form method="POST" action="{{ route('admin.discounts.store') }}" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col">
        <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Name:</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
            <input name="name" value="{{ old('name') }}" type="text" class="form-control">
            </div>
        </div>
        <div class="form-group">
                <label for="value">Discount Value:</label>
                <input type="number" name="value" id="value" class="form-control" required>
            </div>
        </div>
    </div>
    
        
            <div class="form-group">
                <label for="start_date">Start Date:</label>
                <input type="date" name="start_date" id="start_date" class="form-control" required>
            </div>
        
            <div class="form-group">
                <label for="end_date">End Date:</label>
                <input type="date" name="end_date" id="end_date" class="form-control" required>
            </div>
            
            <div class="form-group">
                <label for="apply_to">Apply to:</label>
                <select name="apply_to" id="apply_to" class="form-control" required>
                    <option value="all" >All products</option>
                    <option value="categories">Categories</option>
                    <option value="products">Specific products</option>
                </select>
            </div>
            <div class="form-group" id="categories" style="display:none">
                <label for="category">Categories:</label>
                <select  name="categories[]" id="category" class="form-control" multiple>
                    @foreach($viewData['categories'] as $category)
                        <option value="{{ $category->getId() }}">{{ $category->getName() }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group" id="products" style="display:none">
                <label for="product">Products:</label>
                <select name="products[]" id="product" class="form-control" multiple>
                    @foreach($viewData["products"]  as $product)
                        <option value="{{ $product->id }}">{{ $product->name }}</option>
                    @endforeach
                </select>
            </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    </div>
    </div>
                <div class="card">
                    <div class="card-header">
                        Manage {{$viewData['title']}}
                    </div>
                    <div class="card-body ">
                        <table class="table table-bordered table-striped projects">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Value</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Apply To</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($viewData["discounts"] as $discount)
                                    <tr>
                                        <td>{{ $discount->id }}</td>
                                        <td>{{ $discount->name }}</td>
                                        <td>{{ $discount->value }}</td>
                                        <td>{{ $discount->start_date }}</td>
                                        <td>{{ $discount->end_date }}</td>
                                        <td>{{ $discount->apply_to }}</td>
                                        <td>
                                            <form action="{{ route('admin.discounts.destroy', $discount) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <a class="btn btn-success " href="{{ route('admin.discounts.edit', $discount) }}">
                                                <i class="bi-pencil"></i>
                                                </a>
                                                <button class="btn btn-danger " type="submit">
                                                    <i class="bi-trash"></i> 
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
<script>
    document.getElementById('apply_to').addEventListener('change', function() {
    if (this.value == 'categories') {
        $apply_to=this.value;
        document.getElementById('categories').style.display = 'block';
        document.getElementById('products').style.display = 'none';
    } else if (this.value == 'products') {
        $apply_to=this.value;
        document.getElementById('categories').style.display = 'none';
        document.getElementById('products').style.display = 'block';
    } else {
        $apply_to=this.value;
        document.getElementById('categories').style.display = 'none';
        document.getElementById('products').style.display = 'none';
        console.log(this.value )
    }})
</script>
@endsection
            
