@extends('layouts.admin')
@section('title', $viewData["title"])
@section('content')
<div class="card mb-4">
  <div class="card-header">
    Create Products
  </div>
  <div class="card-body">
    @if($errors->any())
    <ul class="alert alert-danger list-unstyled">
      @foreach($errors->all() as $error)
      <li>- {{ $error }}</li>
      @endforeach
    </ul>
    @endif

    <form method="POST" action="{{ route('admin.product.store') }}" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Name:</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="name" value="{{ old('name') }}" type="text" class="form-control">
            </div>
          </div>
        </div>
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Price:</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="price" value="{{ old('price') }}" type="number" class="form-control">
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Image:</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input class="form-control" type="file" name="image">
            </div>
          </div>
        </div>
        <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">quantity_store:</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input class="form-control" value="{{ old('Quantity') }}" type="number" name="Quantity">
            </div>
          </div>
        </div>
      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label>Category name:</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <select name="category" >
                <option value="{{null}}" >select category</option>
                @foreach($viewData['categories'] as $index=>$category)
                <option value="{{$category->getId()}}">
                  {{$category->getName()}}
                </option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="mb-3 row">
            <label>fornisseur:</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <select name="fornisseur_id" >
                <option value="{{null}}" >select fornisseur</option>
                @foreach($viewData['fornisseurs'] as $index=>$fornisseur)
                <option value="{{$fornisseur->getId()}}">
                  {{$fornisseur->getId()}}
                </option>
                @endforeach
              </select>
            </div>
          </div>
        </div>


        <div class="col">
          &nbsp;
        </div>
      </div>
      <div class="mb-3">
        <label class="form-label">Description</label>
        <textarea class="form-control" name="description" rows="3">{{ old('description') }}</textarea>
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
</div>

<div class="card p-5">
  <div class="card-header">
    Manage Products
  </div>
 

  <div class="row">
 
  <div class="col-md-4 col-lg-3 mb-2">
    <div class="card-body">
      <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
          @csrf
          <input type="file" name="file" class="form-control">
          <br>
          <button class="btn btn-success mb-2">Import Products Data</button>
          <a class="btn btn-warning" href="{{ route('export') }}">Export Products Data</a>
      </form>
  </div>
    
      <form action="{{route('admin.product.index')}}" method="get" class="text-center">
        @csrf
        <select name="fornisseur_id"  class="form-select "  >
          <option value="{{null}}" selected>all</option>
          @foreach($viewData['fornisseurs'] as $index=>$fornisseur)
          <option  value="{{$fornisseur->getId()}}" {{$fornisseur->getId() == $viewData["for_id"] ? "selected": ""}}>
            {{$fornisseur->getId()}}
          </option>
          @endforeach
        </select>
        <button type="submit" class="btn bg-primary text-white  mt-2" style="display:inline">filtrer</button>
      </form>
  </div>
</div>
  <div class="card-body">
    <table class="table table-bordered table-striped ">
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col">Name</th>
          <th scope="col"> Category Name</th>
          <th scope="col">Quantity</th>
          <th scope="col">Id Fornisseur</th>
          <th scope="col">Edit</th>
          <th scope="col">Delete</th>
        </tr>
      </thead>

      <tbody class="bg-success">

        @foreach ($viewData["products"] as $product)
        <div >
        <tr class=" @if($product->getQuantityStore() < 10 ) bg-warning @endif @if($product->getQuantityStore() == 0) bg-danger @endif">
          <td>{{ $product->getId() }}</td>
          <td>{{ $product->getName() }}</td>
          <td>{{$product->Category->getName()}}</td>
          <td>{{ $product->getQuantityStore() }}</td>
          <td>{{ $product->getFornisseurId() }}</td>
          <td>
            <a class="btn btn-primary" href="{{route('admin.product.edit', ['id'=> $product->getId()])}}">
              <i class="bi-pencil"></i>
            </a>
          </td>
          <td>
            <form action="{{ route('admin.product.delete', $product->getId())}}" method="POST">
              @csrf
              @method('DELETE')
              <button class="btn btn-danger">
                <i class="bi-trash"></i>
              </button>
            </form>
          </td>
        </tr>

        </div>
        @endforeach

   

      </tbody>
    </table>
    {{$viewData["products"]->appends(Request::except('page'))->render() }}

    
  </div>
</div>
@endsection
