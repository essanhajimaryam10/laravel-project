<?php

return [
    "online" => "Boutique en ligne",
    "home" => "Acceuil",
    "about" => "àpropos",
    "cart"=> "Chariot",
    "login"=>"connexion",
    "register"=>"enregistrer",
    "products"=>"Produits",
    "title"=>"boutique en ligne laravel",
    "list of products"=>" liste des produits",
    "my orders"=>" Mes commandes ",
    "logout"=>"Se déconnecter"
];
