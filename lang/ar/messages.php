<?php

return [
    "online" => "متجر رقمي",
    "home" => "الرئيسية",
    "about" => "حول",
    "cart"=> "عربة التسوق",
    "login"=>"تسجيل الدخول",
    "register"=>"اضافة حساب",
    "products"=>"منتجات",
    "title"=>"متجر لارافيل على الإنترنت",
    "list of products"=>"قائمة المنتجات",
    "my orders"=>" طلباتي",
    "logout"=>"خروج"

];
