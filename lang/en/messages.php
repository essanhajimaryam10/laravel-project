<?php

return [
    "online" => "Online Store",
    "home" => "Home",
    "about" => "About",
    "products" => "Products",
    "cart" => "Cart",
    "login" => "Login",
    "register" => "Register",
    "title" => "A Laravel Online Store",
    "my orders"=>" my orders",
    "logout"=>"logout"

];
